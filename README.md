# VK Mod 0.7

## Скрипт модификатор для десктопной версии ВКонтакте

Для работы скрипта в браузере должно быть установлено расширение [Tampermonkey](https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo?hl=ru) для Google Chrome или  
[Greasemonkey](https://addons.mozilla.org/ru/firefox/addon/greasemonkey/) для Mozilla Firefox. 

Ссылка для загрузки и обновления скрипта:
[https://bitbucket.org/akman_ru/vk_search_mod/raw/b5e70e456641728aac992a3051211108f2d51ea8/vk_search_mod.user.js](https://bitbucket.org/akman_ru/vk_search_mod/raw/b5e70e456641728aac992a3051211108f2d51ea8/vk_search_mod.user.js)

Скрипт работает **только** на странице ["Поиск"|"Люди"](https://vk.com/search?c%5Bq%5D=&c%5Bsection%5D=people) и **только** в ***десктопной*** версии ВКонтакте!

- Для каждого человека в выдаче добавляется кнопка *ОТКРЫТЬ* для открытия страницы человека в новой вкладке
