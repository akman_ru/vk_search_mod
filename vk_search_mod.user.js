// ==UserScript==
// @name         VK Search Mod
// @namespace    akman.ru
// @version      0.2
// @description  VK desktop version modification script
// @author       A.Kapitman (c) 2016
// @match        *://vk.com/*
// @require      https://code.jquery.com/jquery-3.1.0.min.js
// @updateURL    https://bitbucket.org/akman_ru/vk_search_mod/raw/b5e70e456641728aac992a3051211108f2d51ea8/vk_search_mod.user.js
// @downloadURL  https://bitbucket.org/akman_ru/vk_search_mod/raw/b5e70e456641728aac992a3051211108f2d51ea8/vk_search_mod.user.js
// @grant        none
// ==/UserScript==

(function() {
    'use strict';
	//
    function process() {
        $('.people_row').each(function() {
			var $this = $(this);
			var $controls = $this.find('div.controls').first();
			if (!$controls.find('.vk_search_mod').length) {
				$controls.append('<a class="vk_search_mod flat_button button_small button_wide" style="margin-top: 1em;" href="' +
										 $this.find('.info .name a').first().prop('href') +
										 '" target="_blank">Открыть</a>');
			}
		});
    }
    //
    function update() {
        if (window.location.pathname !== '/search') {
			return;
		}
        $('body').off('DOMSubtreeModified', update);
        process();
        $('body').on('DOMSubtreeModified', update);
    }
	//
    function init() {
		setTimeout(function() {
            $('body').on('DOMSubtreeModified', update);
            update();
		}, 0);
	}
	//
    init();
})();